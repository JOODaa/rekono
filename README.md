# Rekono

DEB package to distribute Rekono Desktop.


## Build package

```bash
dpkg-buildpackage -us -uc
dpkg -i ../rekono_1.6.3_amd64.deb
```

## Execution

```bash
rekono
```

> Default credentials are `rekono:rekono`. For security reasons, **password should be changed** the first time you access the account

## References

- [Rekono](https://github.com/pablosnt/rekono)
- [Rekono CLI](https://github.com/pablosnt/rekono-cli)
- [Rekono image](https://hub.docker.com/r/pablosnt/rekono)
